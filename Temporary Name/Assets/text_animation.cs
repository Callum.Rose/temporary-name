﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class text_animation : MonoBehaviour {

    public float timeTaken;      //Time taken for each letter to appear
    public string message;
    public Text text;

	// Use this for initialization
	void Start ()
    {
        text = GetComponent<Text>();
        message = text.text;
        text.text = "";

        StartCoroutine(TypeText());
	}

    public void ResetText()
    {
        text = GetComponent<Text>();
        message = text.text;
        text.text = "";
        StartCoroutine(TypeText());
    }

    IEnumerator TypeText()
    {
        foreach (char letter in message.ToCharArray())
        {
            text.text += letter;
            yield return 0;
            yield return new WaitForSeconds(timeTaken * Time.deltaTime);
        }
    }
	// Update is called once per frame
	void Update ()
    {
		
	}
}
