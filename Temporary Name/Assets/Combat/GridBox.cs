﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridBox : MonoBehaviour {


    public int value;
    public Vector2Int gridPos;
    public BoxCollider2D boxCollider;
    public CombatGrid combatGrid;

    private void OnMouseDown()
    {
        combatGrid.Move(gridPos);
    }

    private void Update()
    {
        if (value == -1)
        {
            boxCollider.enabled = true;
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }
        else if (value == -2)
        {
            boxCollider.enabled = false;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            //health sprite
        }
        else if (value == -3)
        {
            boxCollider.enabled = true;
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
            //health sprite
        }
        else
        {
            boxCollider.enabled = false;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }

        
    }
}
