﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatGrid : MonoBehaviour {
    #region Globals
    /// <summary>
    /// 0 = unoccupied
    /// 1+ = turn order
    /// -1 = highlighted, can be moved to
    /// -2 = health item
    /// -3 = highlighted + health
    /// </summary>
    public GridBox[,] grid = new GridBox[3, 8];
    public GridBox gridboxPrefab;
    List<CombatObject> turnOrder = new List<CombatObject>();
    public int turn;
    #endregion
    public void Highlight()
    {
        int speed = turnOrder[turn].speed;
        Vector2Int pos = turnOrder[turn].gridPos;

        for (int y = 0; y < 3; ++y)
        {
            for (int x = 0; x < 8; ++x)
            {
                if (grid[y,x].value == 0)
                {
                    if (((y - pos.y)*(y-pos.y) + (x-pos.x)*(x-pos.x)) < speed*speed)
                    {
                        grid[y, x].value = -1;
                    }
                }
                if (grid[y,x].value == -2)
                {
                    if (((y - pos.y) * (y - pos.y) + (x - pos.x) * (x - pos.x)) < speed * speed)
                    {
                        grid[y, x].value = -3;
                    }
                }
            }
        }
    }

    private void Start()
    {
        turnOrder.Add(null);
        float xMod = 1.68f;
        float yMod = 1.68f;
        for (int y = 0; y < 3; ++y)
        {
            for (int x = 0; x < 8; ++x)
            {
                grid[y,x] =  Instantiate(gridboxPrefab, new Vector3(gameObject.transform.position.x + (xMod*(x+1)) + (xMod/2), gameObject.transform.position.y + (yMod * y) + (yMod / 2), gameObject.transform.position.z), Quaternion.identity, gameObject.transform);
                grid[y, x].gridPos = new Vector2Int(y, x);
            }
        }
    }

    public void Populate(int enemies)
    {
        for (int y = 0; y < 3; ++y)
        {
            for (int x = 0; x < 8; ++x)
            {
                grid[y, x].value = 0;
            }
        }

        grid[1, 2].value = 1;
        Player player;
        if (FindObjectOfType<Player>())
        {
            player = FindObjectOfType<Player>();
        }
        else
        {
            player = Instantiate<Player>(FindObjectOfType<CombatController>().character);
        }
        turnOrder.Add(player);
        player.gridPos = new Vector2Int(1, 2);
        player.transform.parent = grid[1, 2].transform;
        player.transform.localPosition = new Vector3(0, 0, 0);

        CombatObject boss;
        if (FindObjectOfType<KeyScript>().gardenKeyCollected)
        {
            turnOrder.Add(Instantiate<Mirror>(FindObjectOfType<CombatController>().mirror));
            boss = FindObjectOfType<Mirror>();
        }
        else if (FindObjectOfType<KeyScript>().basementKeyCollected)
        {
            turnOrder.Add(Instantiate<Teddy>(FindObjectOfType<CombatController>().teddy));
            boss = FindObjectOfType<Teddy>();
        }
        else
        {
            turnOrder.Add(Instantiate<Wife>(FindObjectOfType<CombatController>().wife));
            boss = FindObjectOfType<Wife>();
        }

        for (int i = 2; i < enemies+2; )
        {
            int x = Random.Range(4, 8);
            int y = Random.Range(0, 3);
            if (grid[y,x].value == 0)
            {
                grid[y,x].value = i;
                if (i != 2)
                {
                    GenericFighter genericFighter = Instantiate<GenericFighter>(FindObjectOfType<CombatController>().generic);
                    genericFighter.gridPos = new Vector2Int(y, x);
                    turnOrder.Add(genericFighter);
                    genericFighter.transform.parent = grid[y, x].transform;
                    genericFighter.transform.localPosition = new Vector3(0, 0, 0);
                }
                else
                {
                    boss.gridPos = new Vector2Int(y, x);
                    boss.transform.parent = grid[y, x].transform;
                    boss.transform.localPosition = new Vector3(0, 0, 0);
                }
                ++i;
            }
        }

        for (int i = 0; i < 2; )
        {
            int x = Random.Range(0, 8);
            int y = Random.Range(0, 3);
            if (grid[y,x].value == 0)
            {
                grid[y, x].value = -2;
                ++i;
            }
        }
    }

    public void Move(Vector2Int target)
    {
        Vector2Int pos = turnOrder[turn].gridPos;
        if (grid[target.x,target.y].value == -3)
        {
            ++FindObjectOfType<Player>().healthItems;
        }
        grid[target.x, target.y].value = turn;
        grid[pos.y, pos.x].value = 0;
        turnOrder[turn].gridPos = target;
        turnOrder[turn].transform.parent = grid[target.x, target.y].transform;
    }

    public void RemoveHighlight()
    {
        for (int y = 0; y < 3; ++y)
        {
            for (int x = 0; x < 8; ++x)
            {
                if (grid[y, x].value == -1)
                {
                    grid[y, x].value = 0;
                }
            }
        }
    }

    public void NewTurn()
    {
        turnOrder[turn].OnTurn();
    }

    public void Clear()
    {
        turnOrder.Clear();
        turnOrder.Add(null);
        for (int y = 0; y < 3; ++y)
        {
            for (int x = 0; x < 8; ++x)
            {
                grid[y, x].value = 0;
            }
        }
    }

}
