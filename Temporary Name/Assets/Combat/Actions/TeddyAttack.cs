﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeddyAttack : ActionObject {

    public override void Use()
    {
        FindObjectOfType<Player>().health -= 5;
    }
}
