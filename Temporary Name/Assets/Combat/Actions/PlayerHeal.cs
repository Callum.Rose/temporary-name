﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHeal : ActionObject {

    public override void Use()
    {
        FindObjectOfType<Player>().target.health += 1;
        --FindObjectOfType<Player>().healthItems;
    }
}
