﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WifeHeal : ActionObject {

    public override void Use()
    {
        ++FindObjectOfType<Wife>().health;
    }
}
