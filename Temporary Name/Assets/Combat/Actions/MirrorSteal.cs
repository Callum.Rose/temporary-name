﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorSteal : ActionObject {

    public override void Use()
    {
        --FindObjectOfType<Player>().healthItems;
        ++FindObjectOfType<Mirror>().health;
    }
}
