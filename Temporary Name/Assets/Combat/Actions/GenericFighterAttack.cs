﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericFighterAttack : ActionObject {

	public override void Use()
    {
        FindObjectOfType<Player>().health -= 1;
    }
}
