﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : ActionObject {

    public override void Use()
    {
        FindObjectOfType<Player>().target.health -= 1;
    }
}
