﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericFighter : CombatObject {

	public override void OnTurn()
    {
        int movesLeft = speed;
        Player player = FindObjectOfType<Player>();
        CombatGrid grid = FindObjectOfType<CombatGrid>();
        /*#region Movement
        if (player.gridPos.x < gridPos.x - 1)
        {
            if (grid.grid[gridPos.x - 1, gridPos.y].value < 1)
            {
                grid.Move(new Vector2Int(gridPos.x - 1, gridPos.y));
                --movesLeft;
            }
        }
        if (movesLeft != 0)
        {
            if (player.gridPos.y < gridPos.y - 1)
            {
                if (grid.grid[gridPos.x, gridPos.y-1].value < 1)
                {
                    grid.Move(new Vector2Int(gridPos.x, gridPos.y-1));
                    --movesLeft;
                }
            }
        }
        if (movesLeft != 0)
        {
            if (player.gridPos.y > gridPos.y + 1)
            {
                if (grid.grid[gridPos.x, gridPos.y + 1].value < 1)
                {
                    grid.Move(new Vector2Int(gridPos.x, gridPos.y + 1));
                    --movesLeft;
                }
            }
        }
        if (movesLeft != 0)
        {
            if (player.gridPos.x > gridPos.x + 1)
            {
                if (grid.grid[gridPos.x + 1, gridPos.y].value < 1)
                {
                    grid.Move(new Vector2Int(gridPos.x + 1, gridPos.y));
                    --movesLeft;
                }
            }
        }
        #endregion*/
        #region Attack

        gameObject.GetComponent<Animator>().SetBool("isAttacking", true);
        gameObject.GetComponent<Animator>().ResetTrigger("isAttacking");
            
        #endregion
    }
    void Start()
    {
        health = 1;
        speed = 1;
        action1 = Instantiate<GenericFighterAttack>(FindObjectOfType<CombatController>().genericFighterAttack);
        
    }
}
