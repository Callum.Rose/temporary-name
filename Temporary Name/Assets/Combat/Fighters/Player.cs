﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : CombatObject {

    public bool playerTurn = false;
    public CombatObject target;
    public int healthItems;

    public override void OnTurn()
    {
        playerTurn = true;
    }

    void Start()
    {
        speed = 2;
        healthItems = 0;
        health = 25;
        action1 = Instantiate<PlayerAttack>(FindObjectOfType<CombatController>().playerAttack);
        action2 = Instantiate<PlayerHeal>(FindObjectOfType<CombatController>().playerHeal);
    }
}
