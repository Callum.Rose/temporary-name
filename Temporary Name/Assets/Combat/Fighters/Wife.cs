﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wife : CombatObject {

    bool healed = false;
    public override void OnTurn()
    {

        
        int movesLeft = speed;
        Player player = FindObjectOfType<Player>();
        CombatGrid grid = FindObjectOfType<CombatGrid>();
        //#region Movement
        //if (player.gridPos.x < gridPos.x - 1)
        //{
        //    if (grid.grid[gridPos.x - 1, gridPos.y].value < 1)
        //    {
        //        grid.Move(new Vector2Int(gridPos.x - 1, gridPos.y));
        //        --movesLeft;
        //    }
        //}
        //if (movesLeft != 0)
        //{
        //    if (player.gridPos.y < gridPos.y - 1)
        //    {
        //        if (grid.grid[gridPos.x, gridPos.y - 1].value < 1)
        //        {
        //            grid.Move(new Vector2Int(gridPos.x, gridPos.y - 1));
        //            --movesLeft;
        //        }
        //    }
        //}
        //if (movesLeft != 0)
        //{
        //    if (player.gridPos.y > gridPos.y + 1)
        //    {
        //        if (grid.grid[gridPos.x, gridPos.y + 1].value < 1)
        //        {
        //            grid.Move(new Vector2Int(gridPos.x, gridPos.y + 1));
        //            --movesLeft;
        //        }
        //    }
        //}
        //if (movesLeft != 0)
        //{
        //    if (player.gridPos.x > gridPos.x + 1)
        //    {
        //        if (grid.grid[gridPos.x + 1, gridPos.y].value < 1)
        //        {
        //            grid.Move(new Vector2Int(gridPos.x + 1, gridPos.y));
        //            --movesLeft;
        //        }
        //    }
        //}
        //#endregion
        #region Attack
        if (health == 1 && !healed)
        {
            UseAction(action2);
            healed = true;
            FindObjectOfType<CombatController>().NextTurn();
        }
        else
        {
            gameObject.GetComponent<Animator>().SetBool("isAttacking", true);
            gameObject.GetComponent<Animator>().ResetTrigger("isAttacking");
            healed = false;
        }
        #endregion
    }

    void Start()
    {
        health = 3;
        speed = 2;
        action1 = Instantiate<WifeAttack>(FindObjectOfType<CombatController>().wifeAttack);
        action2 = Instantiate<WifeHeal>(FindObjectOfType<CombatController>().wifeHeal);
        
    }
}