﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Combat actions inherit from this
/// </summary>
public abstract class ActionObject : MonoBehaviour {

    public abstract void Use();
}
