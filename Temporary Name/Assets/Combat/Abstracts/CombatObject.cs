﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Fighters inherit from this
/// </summary>
public abstract class CombatObject : MonoBehaviour {

    #region Globals
    public double health;
    public int speed;
    public Vector2Int gridPos;
    public ActionObject action1;
    public ActionObject action2;
    public ActionObject action3;
    public ActionObject action4;

    public Collider2D collider;
    #endregion

    public void UseAction(ActionObject action)
    {
        action.Use();
    }

    public abstract void OnTurn();

    private void Update()
    {
        if (health == 0)
        {
            gameObject.GetComponent<Animator>().SetBool("isDead", true);
        }
    }

    private void OnMouseDown()
    {
        FindObjectOfType<Player>().target = this;
        //Action Interface
        if (FindObjectOfType<Player>() == FindObjectOfType<Player>().target)
        {
            UseAction(action2);
            FindObjectOfType<CombatController>().NextTurn();
        }
        else
        {
            //FindObjectOfType<Player>().GetComponent<Animator>().SetBool("isAttacking", true);
            FindObjectOfType<Player>().GetComponent<Animator>().ResetTrigger("isAttacking");
        }
    }

    public void DeathAnimationEnd()
    {
        Destroy(gameObject);
    }

    public void AttackAnimationEnd()
    {
        UseAction(action1);
        FindObjectOfType<CombatController>().NextTurn();
        gameObject.GetComponent<Animator>().SetTrigger("isAttacking");
    }
}
