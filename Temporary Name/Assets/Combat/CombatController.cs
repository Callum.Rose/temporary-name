﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatController : MonoBehaviour {

    #region Globals
    int turn;
    int enemies;
    public CombatGrid combatGrid;
    public GameObject combatContainer;
    public Player character;
    public Wife wife;
    public Teddy teddy;
    public Mirror mirror;
    public GenericFighter generic;
    public GenericFighterAttack genericFighterAttack;
    public MirrorAttack mirrorAttack;
    public MirrorSteal mirrorSteal;
    public PlayerAttack playerAttack;
    public PlayerHeal playerHeal;
    public TeddyAttack teddyAttack;
    public WifeAttack wifeAttack;
    public WifeHeal wifeHeal;
    public bool inCombat = false;
    #endregion
    public void StartCombat()
    {
        inCombat = true;
        FindObjectOfType<Player_Movement>().cancelMovement = true;
        FindObjectOfType<Player>().GetComponent<Animator>().SetBool("isWalking", false);
        FindObjectOfType<Player>().GetComponent<Animator>().SetBool("isFighting", true);
        turn = 0;
        enemies = Random.Range(1, 5);
        combatGrid.Populate(enemies);
        NextTurn();
    }

    public void NextTurn()
    {
        combatGrid.RemoveHighlight();
        ++turn;
        if (turn>enemies)
        {
            turn = 1;
        }
        combatGrid.turn = turn;
        combatGrid.NewTurn();
    }

    public void EndCombat()
    {
        inCombat = false;
        combatGrid.Clear();
        FindObjectOfType<Player>().transform.parent = null;
        FindObjectOfType<BattleTrigger>().isActive = false;
        //remove UI Stuff
        combatContainer.SetActive(false);
        FindObjectOfType<Player_Movement>().cancelMovement = false;
        FindObjectOfType<Player>().GetComponent<Animator>().SetBool("isFighting", false);
    }
}
