﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionIcon : MonoBehaviour {

    public ActionObject action;
    public CircleCollider2D circleCollider2D;

    private void OnMouseDown()
    {
        FindObjectOfType<Player>().UseAction(action);
    }
}
