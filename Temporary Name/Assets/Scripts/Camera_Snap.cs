﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Snap : MonoBehaviour {

    public Camera mainCam;
    public Vector3 snapPos;
    public Animator bossAnim;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            mainCam.transform.position = snapPos;
            bossAnim.SetTrigger("hasTriggered");
        }
    }
}
