﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDescription : MonoBehaviour {

    //list of descriptions
    public List<string> descriptions = new List<string>();

    //return the list of descriptions for the text manager
    public List<string> GetDescriptions()
    {
        return descriptions;
    }
}
