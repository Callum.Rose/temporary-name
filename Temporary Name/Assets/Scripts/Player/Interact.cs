﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interact : MonoBehaviour {


    private Canvas mainCanvas;
    public GameObject pressASprite;
    Player_Movement playerMovement;
    private KeyScript playerKeyScript;
    //For the main dialogue bubble
    public GameObject speechBubble;
    private GameObject tempSpeechBubble;
    text_animation speechBubbleAnimation;
    ObjectDescription objectDescription;
    private int speechCounter;
    List<string> descriptions = new List<string>();
    bool currentlyInteracting = false;
    bool currentlyColliding = false;
    Text speechBubbleText;
    int currentDescription = 0;


    // Use this for initialization
    void Start ()
    {
        speechCounter = 0;
        mainCanvas = FindObjectOfType<Canvas>();
        playerKeyScript = GetComponent<KeyScript>();
        playerMovement = GetComponent<Player_Movement>();
        playerMovement.cancelMovement = false;
        descriptions.Add("Can you please work it would be really nice part 1");
        descriptions.Add("I'm not sure if this will work but if it does it'll be gucci");
        descriptions.Add("YEEEEEEEEEEEEEEEEEEEEEEEET");
    }
	
	// Update is called once per frame
	void Update ()
    {
        //if an object is currently being interacted with and a window is open
        if (currentlyInteracting)
        {
            playerMovement.cancelMovement = true;
            //iterate through the descriptions from the object we're interacting with
            InteractWithObject();
        }
        //if an object is not being interacted with
        else if (!FindObjectOfType<CombatController>().inCombat)
        {
            playerMovement.cancelMovement = false;
        }
        pressASprite.SetActive(currentlyColliding);

    }

    public void InteractWithObject()
    {
        //if the A key is down
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //if we've not reached the max amount of descriptions
            if (currentDescription < descriptions.Count -1)
            {
                //see next description and reset animation
                currentDescription++;
                speechBubbleText.text = descriptions[currentDescription];
                speechBubbleAnimation.ResetText();
            }
            else
            {
                //Destroy the speechbubble we instantiated
                Destroy(tempSpeechBubble);
                currentDescription = 0;
                currentlyInteracting = false;
            }
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        //if we've collided with an object and we're not currently interacting
        if ((collision.gameObject.tag == "Object" || collision.gameObject.tag == "BabyDoor" || collision.gameObject.tag == "BasementDoor") && currentlyInteracting == false)
        {
            currentlyColliding = true;
            //if E key is pressed
            if (Input.GetKeyDown(KeyCode.E))
            {

                //Create an interaction window
                tempSpeechBubble = Instantiate(speechBubble, (mainCanvas.transform.position + new Vector3(0.0f, 0.0f, 0.0f)), Quaternion.identity, mainCanvas.transform);
                tempSpeechBubble.transform.localPosition = new Vector3(0.0f, -200.0f, 0.0f);
                //Get the object descriptions from the collided object
                objectDescription = collision.gameObject.GetComponent<ObjectDescription>();
                descriptions = objectDescription.GetDescriptions();

                //Get the text, text animation and descriptions from the object
                speechBubbleText = tempSpeechBubble.GetComponentInChildren<Text>();
                speechBubbleAnimation = tempSpeechBubble.GetComponentInChildren<text_animation>();
                speechBubbleText.text = descriptions[currentDescription];
                speechBubbleAnimation.ResetText();
                currentlyInteracting = true;





            }
        }
        else if ((collision.gameObject.tag == "BattleTrigger" && currentlyInteracting == false))
        {
            currentlyColliding = true;
            if (Input.GetKeyDown(KeyCode.E))
            {
                collision.gameObject.GetComponent<BattleTrigger>().Interact();
            }
        }
        if (collision.gameObject.tag == "KitchenKey" && currentlyInteracting == false)
        {
            currentlyColliding = true;
            if (Input.GetKeyDown(KeyCode.E))
            {
                FindObjectOfType<BattleTrigger>().isActive = true;
                playerKeyScript.childKeyCollected = true;
                //Create an interaction window
                tempSpeechBubble = Instantiate(speechBubble, (mainCanvas.transform.position + new Vector3(0.0f, 0.0f, 0.0f)), Quaternion.identity, mainCanvas.transform);
                tempSpeechBubble.transform.localPosition = new Vector3(0.0f, -200.0f, 0.0f);
                //Get the object descriptions from the collided object
                objectDescription = collision.gameObject.GetComponent<ObjectDescription>();
                descriptions = objectDescription.GetDescriptions();

                //Get the text, text animation and descriptions from the object
                speechBubbleText = tempSpeechBubble.GetComponentInChildren<Text>();
                speechBubbleAnimation = tempSpeechBubble.GetComponentInChildren<text_animation>();
                speechBubbleText.text = descriptions[currentDescription];
                speechBubbleAnimation.ResetText();
                currentlyInteracting = true;

                Destroy(collision.gameObject);
            }
        }
        if (collision.gameObject.tag == "BabyKey" && currentlyInteracting == false)
        {
            currentlyColliding = true;
            if (Input.GetKeyDown(KeyCode.E))
            {
                FindObjectOfType<BattleTrigger>().isActive = true;
                playerKeyScript.basementKeyCollected = true;
                //Create an interaction window
                tempSpeechBubble = Instantiate(speechBubble, (mainCanvas.transform.position + new Vector3(0.0f, 0.0f, 0.0f)), Quaternion.identity, mainCanvas.transform);
                tempSpeechBubble.transform.localPosition = new Vector3(0.0f, -200.0f, 0.0f);
                //Get the object descriptions from the collided object
                objectDescription = collision.gameObject.GetComponent<ObjectDescription>();
                descriptions = objectDescription.GetDescriptions();

                //Get the text, text animation and descriptions from the object
                speechBubbleText = tempSpeechBubble.GetComponentInChildren<Text>();
                speechBubbleAnimation = tempSpeechBubble.GetComponentInChildren<text_animation>();
                speechBubbleText.text = descriptions[currentDescription];
                speechBubbleAnimation.ResetText();
                currentlyInteracting = true;



                Destroy(collision.gameObject);
            }
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        currentlyColliding = false;
        Destroy(tempSpeechBubble);
        speechCounter = 0;
    }
}
