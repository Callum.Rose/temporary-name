﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour {

    public bool cancelMovement = false;
    public float speed;               //speed variable for player movement
    bool hasFlipped;                //boolean to check whether the player's sprite has been flipped or not 

    private Animator animator;
    public List<AudioClip> walkSounds;
    private AudioSource playerAudio;
    public float waitTime;
    public float waitTimeMax;
    private int walkIterator;


    private Vector3 scale;


    // Use this for initialization
    void Start ()
    {
        //the player's sprite will not be flipped on start up
        hasFlipped = false;
        scale = gameObject.transform.localScale;
        animator = this.GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (cancelMovement != true)
        {

            Movement();
            FlipSprite();
        }
        else
        {
            animator.SetTrigger("isIdle");
        }
	}

    void Movement()
    {
        //get the horizontal inputs on either the keyboard or controller
        float moveHorizontal = Input.GetAxis("Horizontal");
        //get the vertical inputs on either the keyboard or controller
        float moveVertical = Input.GetAxis("Vertical");

        //move the player in a user specified direction 
        gameObject.transform.Translate(moveHorizontal * speed * Time.deltaTime, moveVertical * speed * Time.deltaTime, 0.0f);

        if ((float)(moveHorizontal * speed * Time.deltaTime) <= 0.1f &&((float)(moveVertical * speed * Time.deltaTime) <= 0.1f))
        {
            animator.SetBool("isWalking", false);
        }

        waitTime += Time.deltaTime;

        PlayWalkingSounds(moveVertical, moveHorizontal);
    }
    void FlipSprite()
    {
        //if the player is moving in the positive x axis then flip the sprite
        if (Input.GetAxis("Horizontal") > 0.0f)
        {
            animator.SetBool("isWalking", true);
            if (!hasFlipped)
            {
                


                gameObject.transform.localScale = new Vector3(-scale.x, scale.y, scale.z);


                hasFlipped = true;
            }
        }
        //if the player is moving in the negative x axis and the player sprite has already been flipped then flip it back 
        else if (Input.GetAxis("Horizontal") < 0.0f)
        {
            animator.SetBool("isWalking", true);
            if (hasFlipped)
            {
                gameObject.transform.localScale = scale;
                hasFlipped = false;
            }
        }
        else if (Input.GetAxis("Horizontal") == 0.0f)
        {
            
        }
    }

    void PlayWalkingSounds(float moveVertical, float moveHorizontal)
    {
        if ((moveVertical != 0.0f || moveHorizontal != 0.0f) && waitTime > waitTimeMax)
        {
            playerAudio.PlayOneShot(walkSounds[walkIterator]);
            walkIterator++;
            waitTime = 0.0f;

            if (walkIterator == 2)
            {
                walkIterator = 0;
            }
        }
    }
}
