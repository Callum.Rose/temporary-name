﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Transition : MonoBehaviour {


    public GameObject player;
    public Camera mainCam;
    public Vector2 camShift;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Hit");

        if (collision.gameObject.tag == "Player")
        {
            mainCam.transform.Translate(new Vector3(camShift.x, camShift.y, 0.0f));
        }
    }
}
