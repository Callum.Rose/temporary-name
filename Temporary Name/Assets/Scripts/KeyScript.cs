﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript : MonoBehaviour {
    public bool childKeyCollected = false;
    public bool basementKeyCollected = false;
    public bool gardenKeyCollected = false;

    private GameObject basementDoor;
    private GameObject babyDoor;

    private void Start()
    {
        basementDoor = GameObject.FindGameObjectWithTag("BasementDoor");
        babyDoor = GameObject.FindGameObjectWithTag("BabyDoor");
    }


    private void Update()
    {
        if (childKeyCollected == true && babyDoor.activeInHierarchy == true)
        {
            babyDoor.SetActive(false);
        }
        if (basementKeyCollected == true && basementDoor.activeInHierarchy == true)
        {
            basementDoor.SetActive(false);
        }
        if (gardenKeyCollected == true)
        {

        }
        
    }
}
