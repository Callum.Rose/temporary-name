﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music_Functions : MonoBehaviour {

    public List<AudioClip> musicList;
    public bool isBattleScene;

    private AudioSource bgMusic;
    private float fadeTime;



    bool hasFaded;
	// Use this for initialization
	void Start ()
    {
        bgMusic = GetComponent<AudioSource>();

        fadeTime = Time.deltaTime / 15;
    }

   public void FadeOut()
    {
        bgMusic.volume -= fadeTime;
    }

   public void FadeIn()
    {
        bgMusic.volume += fadeTime;
    }

    public void SetMusic(AudioClip music)
    {
        bgMusic.clip = music;
    }

    public void PlayMusic()
    {
        bgMusic.Play();
    }

    public AudioSource GetAudioSource()
    {
        return bgMusic;
    }



/*
 * 
 * 
    void Transition()
    {
        if (!hasFaded && bgMusic.clip != musicList[sceneCounter])
        {
            FadeOut();
        }

        if (bgMusic.volume == 0.0f)
        {
            hasFaded = true;
            SetMusic(musicList[sceneCounter]);
            bgMusic.Play();
        }
        else if (bgMusic.volume >= 0.1f && hasFaded)
        {
            bgMusic.volume = 0.1f;
            hasFaded = false;
        }

        if (hasFaded)
        {
            FadeIn();
        }
    }

    */
}
