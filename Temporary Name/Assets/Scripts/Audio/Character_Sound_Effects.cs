﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_Sound_Effects : MonoBehaviour {

    public int deathSoundPos;  
    public int minHitRange;
    public int maxHitRange;
    public int hurtSoundPos;
    public int minGrowlRange;
    public int maxGrowlRange;
   
    public List<AudioClip> characterSoundEffects;
    public List<AudioClip> basicEnemySoundFX;
    public List<AudioClip> wifeSoundFX;
    public List<AudioClip> dollSoundFX;
    public List<AudioClip> shadowSoundFX;

    private AudioSource charEffects;

    private List<AudioClip> selectedSoundFX;
	// Use this for initialization
	void Start () {
        charEffects = GetComponent<AudioSource>();

        if (characterSoundEffects.Count > 0)
        {
            selectedSoundFX = characterSoundEffects;
        } 
	}

    public void PlayDeath()
    {
        charEffects.PlayOneShot(selectedSoundFX[deathSoundPos]);
    }

    public void PlayHit()
    {
        charEffects.PlayOneShot(selectedSoundFX[Random.Range(minHitRange, maxHitRange)]);      
    }

    public void PlayHurt()
    {
        charEffects.PlayOneShot(selectedSoundFX[hurtSoundPos]);
    }

    public void PlayGrowl()
    {
        charEffects.PlayOneShot(selectedSoundFX[Random.Range(minGrowlRange, maxGrowlRange)]);
    }
}
