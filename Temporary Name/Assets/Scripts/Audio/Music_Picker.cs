﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music_Picker : MonoBehaviour {

    public AudioClip music;
    public GameObject bgMusic;

    private Music_Functions musicFunctions;

    bool isFading;
    bool hasTriggered;
    bool hasChanged;

    // Use this for initialization
    void Start () {

        //get the music script from the background music game object and store it for referencing 
        musicFunctions = bgMusic.GetComponent<Music_Functions>();

	}

    private void Update()
    {
        //if the player has triggered the battle scene
        if (hasTriggered)
        {
            if (isFading)
            {
                //fade the current music out
                musicFunctions.FadeOut();

                //if the music has finished fading stop 
                if (musicFunctions.GetAudioSource().volume == 0)
                {
                    isFading = false;
                }
            }
            else if (!isFading)
            {
                if (!hasChanged)
                {
                    //if the music has stopped fading but has not changed the current music change it and play it
                    musicFunctions.SetMusic(music);
                    musicFunctions.PlayMusic();
                    hasChanged = true;
                }

                //if the music has not faded back in call the fade in
                if (musicFunctions.GetAudioSource().volume <= 0.1f)
                {
                    musicFunctions.FadeIn();
                }
                //once the music has faded in destroy the object so it cannot be triggered again
                else if (musicFunctions.GetAudioSource().volume >= 0.1f)
                {
                    musicFunctions.GetAudioSource().volume = 0.1f;
                    Destroy(gameObject);

                }
            }
        }
    }



    void OnTriggerEnter2D(Collider2D collision)
    {
        //if the player collides with the trigger point then trigger music transition
        if (collision.gameObject.tag == "Player")
        {
            isFading = true;
            hasTriggered = true;
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        //when the player exits the collider desstroy it so it cannot be triggered again
        Destroy(gameObject.GetComponent<BoxCollider2D>());
       
    }
}
