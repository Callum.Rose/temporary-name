﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneController : MonoBehaviour {

    private Animator cutsceneAnimator;
    public GameObject player;


    public float time;
    public float normalisedTime;

	// Use this for initialization
	void Start () {
        cutsceneAnimator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (cutsceneAnimator.GetCurrentAnimatorStateInfo(0).IsName("Cutscene1"))
        {
            normalisedTime = cutsceneAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            time += Time.deltaTime;
            CutSceneOne();
        }



	}

    void CutSceneOne()
    {
        if (time > cutsceneAnimator.GetCurrentAnimatorStateInfo(0).length)
        {
            Destroy(GameObject.Find("Cutscene1"));

            player.SetActive(true);
        }
    }

}
